package com.swc.stianstrange.kotlin

import android.app.AlertDialog
import android.content.pm.PackageManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.util.Log
import android.widget.Toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_big_product.*
import org.jetbrains.anko.email
import org.jetbrains.anko.makeCall
import org.jetbrains.anko.sendSMS

class BigProductActivity : AppCompatActivity() {
    private val TAG: String = "BigPictureLog"
    val CALL_REQUEST_CODE = 1001



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_big_product)
        setupPermissions()

        val bundle = intent.extras
        if (bundle != null) {
            bigProductTitle.text = bundle.getString("ShowNoteTitle")
            bigProductContent.text = bundle.getString("ShowNoteContent")
            bigProductCity.text = bundle.getString("ShowNoteCity")
            bigProductPrice.text = bundle.getString("ShowNotePrice")
            bigProductEmail.text = bundle.getString("ShowNoteEmail")
            bigProductNumber.text = bundle.getString("ShowNoteTelephone")
            val picture = bundle.getString("showNotePicture")
            Log.i(TAG, "Following URI to picture is parsed from Bundle: " + picture)
            if (bundle.getString("showNotePicture").length < 4) {
                Toast.makeText(
                    applicationContext,
                    "Ingen bilde tilknyttet produktet",
                    Toast.LENGTH_SHORT
                ).show()
                Log.w(TAG, "No picture path in bundle")
            } else {
                Picasso.with(this)
                    .load(bundle.getString("showNotePicture"))
                    .fit()
                    .centerCrop()
                    .rotate(90F)
                    .into(bigproductPicture)
                Log.d(TAG, picture + " is added to bigproductPicture")
            }
        }
        val email = bundle.getString("ShowNoteEmail")
        val tlf = bundle.getString("ShowNoteTelephone")
        val message = bundle.getString("ShowNoteTitle")
       // Log.d(TAG, tlf)
        bigProductNumber.setOnClickListener {

            val builder = AlertDialog.Builder(this, R.style.AlertDialog_AppCompat)
            builder.setTitle("Ring eller send SMS")
            builder.setMessage("Velger du å gå videre vil du ringe eller sende en sms til følgende telefonnummer: $tlf")
            //builder.setPositiveButton("OK", DialogInterface.OnClickListener(function = x))

            builder.setPositiveButton("Ring") { dialog, which ->
            makeCall(tlf)
            }

            builder.setNegativeButton("SMS") { dialog, which ->
                sendSMS(tlf,"Hei! Er det mulig å låne $message ? Jeg ser i utlånsappen at du låner ut dette, stemmer dette? ")
            }
            builder.setNeutralButton("Avbryt") {dialog, which ->
            }
            builder.show()
        }
        bigProductEmail.setOnClickListener {
            email(email, "Ønske om å låne $message", "Hei! Jeg ser i utlånsappen at du låner ut $message, stemmer dette? Jeg er interessert i å vite mer om dette og om det er mulig å låne?")
        }

    }
    fun setupPermissions() {

        val permission = ContextCompat.checkSelfPermission(this,
            android.Manifest.permission.CALL_PHONE)
        if (permission != PackageManager.PERMISSION_GRANTED) {
            makeRequest()
        }

    }
    fun makeRequest() {
        ActivityCompat.requestPermissions(this,
            arrayOf(android.Manifest.permission.CALL_PHONE),
            CALL_REQUEST_CODE)
    }
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        when (requestCode) {
            CALL_REQUEST_CODE -> {

                if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED) {

                    Toast.makeText(this,"Permission Denied",Toast.LENGTH_SHORT).show()

                } else {
                    Toast.makeText(this,"Permission Granted",Toast.LENGTH_SHORT).show()

                }
            }
        }
    }
}




