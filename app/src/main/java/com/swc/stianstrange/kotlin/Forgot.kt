package com.swc.stianstrange.kotlin

import android.app.Activity
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import kotlinx.android.synthetic.main.forgot.*

class Forgot : AppCompatActivity(), View.OnClickListener {


    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.forgot)
        buttonForgotOnForgot.setOnClickListener(this)
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)

    }

    override fun onClick(v: View?) {
        val i=v?.id
        when (i){
            R.id.buttonForgotOnForgot -> forgotpw(editTextEmailForgot.text.toString())
        }
    }
    private fun forgotpw (email: String){
        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        var emailstring = editTextEmailForgot.text.toString()
        if (TextUtils.isEmpty(emailstring)){
            editTextEmailForgot.error = "Påkrevd"
        }else{
        buttonForgotOnForgot.text ="Loader"
        auth.sendPasswordResetEmail(email).addOnCompleteListener(this) {
            task -> if (task.isSuccessful){
            Log.d(TAG,"EMAIL VERIFICATION SENDT")
            Toast.makeText(baseContext,"Passord reset epost sendt!", Toast.LENGTH_SHORT).show()
            buttonForgotOnForgot.text= "Fullført"
        } else {
            Log.d(TAG,"EMAIL VERIFICATION NOT SENDT, ERROR:" + task.exception)
            Toast.makeText(baseContext,"En har oppstått, prøv igjen eller kontakt support",Toast.LENGTH_LONG).show()
            }
        }
    }}

    companion object {
        private const val TAG = "FirebaseAuthLog"
    }
}
