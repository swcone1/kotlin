package com.swc.stianstrange.kotlin

import android.app.ActivityOptions
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.google.android.gms.ads.AdRequest
import com.google.android.gms.ads.AdView
import com.google.android.gms.ads.InterstitialAd
import com.google.android.gms.ads.MobileAds
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import com.google.firebase.inappmessaging.FirebaseInAppMessaging
import kotlinx.android.synthetic.main.activity_main2.*
import com.google.firebase.perf.FirebasePerformance


class MainActivity : AppCompatActivity() {

    private val TAG = "MyLog"
    //RecyclerAdapter
    private var mAdapter: NoteRecyclerViewAdapter? = null

    //FireStore
    private var firestoreDB: FirebaseFirestore? = null
    private var firestoreListener: ListenerRegistration? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics
    private lateinit var mInterstitialAd: InterstitialAd
    lateinit var mAdView : AdView
    private var FirebaseInAppMessaging: FirebaseInAppMessaging? = null




    public override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    public override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }


    public override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.show()
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        FirebaseInAppMessaging?.setMessagesSuppressed(false)

        super.onCreate(savedInstanceState)
        Log.d(TAG, "onCreate() called")
        MobileAds.initialize(this,"ca-app-pub-2106018268188752~5370685919")
        mInterstitialAd = InterstitialAd(this)
        mInterstitialAd.adUnitId = "ca-app-pub-2106018268188752/6129890353"
        mInterstitialAd.loadAd(AdRequest.Builder().build())
        mInterstitialAd.show()


        setContentView(R.layout.activity_main2)
        //Firebase initialzation
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        firestoreDB = FirebaseFirestore.getInstance()
        val authcurrentuser = auth.currentUser.toString()

        mAdView = findViewById(R.id.banner)
        val adRequest = AdRequest.Builder().build()
        mAdView.loadAd(adRequest)

        //Logs
        Log.i(TAG, "FireStoreinstance: $firestoreDB")
        Log.i(TAG, "FirebaseAuthInstance: $auth")
        Log.i(TAG, "FirebaseAnalyticsInstance: $analyt")
        Log.i(TAG, "Current logged in user : $authcurrentuser")

        //Get current list of products
        var cityvar = ""
        firestoreDB!!.collection("notes").orderBy("title")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val notesList = mutableListOf<Note>()

                    for (doc in task.result!!) {
                        val note = doc.toObject<Note>(Note::class.java)
                        note.id = doc.id
                        notesList.add(note)
                    }
                    //progressBar.visibility = View.GONE
                    Log.d(TAG, "Progressbar GONE at line 126")

                    mAdapter = NoteRecyclerViewAdapter(notesList, applicationContext, firestoreDB!!)
                    val mLayoutManager = LinearLayoutManager(applicationContext)
                    rvNoteList.layoutManager = mLayoutManager
                    rvNoteList.itemAnimator = DefaultItemAnimator()
                    rvNoteList.adapter = mAdapter
                } else {
                    //progressBar.visibility = View.GONE
                    Log.d(TAG, "Progressbar GONE at line 135")
                    Log.d(TAG, "Error getting documents: ", task.exception)
                    Toast.makeText(baseContext,"En feil oppstod, vennligst prøv på nytt", Toast.LENGTH_LONG).show()
                }
            }
        searchbutton.setOnClickListener {
            if (searchCity.text.length >= 1){
        loadNotesList()}else{
                loadNotesListStandard()
            }
        }


        Log.d(TAG,"LoadNotesList initialized")
        /*val notestrace1 = FirebasePerformance.getInstance().newTrace("NotesTrace1")
        notestrace1.start()
        firestoreListener = firestoreDB!!.collection("notes").whereGreaterThanOrEqualTo("city",cityvar)
                .addSnapshotListener(EventListener { documentSnapshots, e ->
                    if (e != null) {
                        Log.e(TAG, "Listen failed!", e)
                        return@EventListener
                    }

                    val notesList = mutableListOf<Note>()

                    for (doc in documentSnapshots!!) {
                        val note = doc.toObject(Note::class.java)
                        note.id = doc.id
                        notesList.add(note)
                        Log.d(TAG, "message: $notesList")
                    }

                    mAdapter = NoteRecyclerViewAdapter(notesList, applicationContext, firestoreDB!!)
                    rvNoteList.adapter = mAdapter
                })
        notestrace1.stop()*/

    }


    override fun onDestroy() {
        super.onDestroy()
        //firestoreListener!!.remove()
    }
    
    
    //NEED VERIFICATION
    findViewById<EditText>(R.id.searchCity).setOnEditorActionListener { v, actionId, event ->
    return@setOnEditorActionsListener when (actionID) {
        EditorInfo.IME_ACTION_SEARCH -> {
        loadNotesList()
        }
        }
        }
    

    private fun loadNotesListStandard() {
        //progressBar.visibility = View.VISIBLE
        val notestrace2 = FirebasePerformance.getInstance().newTrace("NotesTrace2")
        notestrace2.start()
        Log.d(TAG, "Progressbar VISIBLE at line 112")

        var cityvarstart = searchCity.text.toString().toUpperCase()

        firestoreDB!!.collection("notes").orderBy("title")
            .get()
            .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val notesList = mutableListOf<Note>()

                    for (doc in task.result!!) {
                        val note = doc.toObject<Note>(Note::class.java)
                        note.id = doc.id
                        notesList.add(note)
                    }
                    //progressBar.visibility = View.GONE
                    Log.d(TAG, "Progressbar GONE at line 126")

                    mAdapter = NoteRecyclerViewAdapter(notesList, applicationContext, firestoreDB!!)
                    val mLayoutManager = LinearLayoutManager(applicationContext)
                    rvNoteList.layoutManager = mLayoutManager
                    rvNoteList.itemAnimator = DefaultItemAnimator()
                    rvNoteList.adapter = mAdapter
                } else {
                    //progressBar.visibility = View.GONE
                    Log.d(TAG, "Progressbar GONE at line 135")
                    Log.d(TAG, "Error getting documents: ", task.exception)
                    Toast.makeText(baseContext,"En feil oppstod, vennligst prøv på nytt", Toast.LENGTH_LONG).show()
                }
            }
        notestrace2.stop()

    }

    private fun loadNotesList() {
        //progressBar.visibility = View.VISIBLE
        val notestrace2 = FirebasePerformance.getInstance().newTrace("NotesTrace2")
        notestrace2.start()
        Log.d(TAG, "Progressbar VISIBLE at line 112")

        var cityvarstart = searchCity.text.toString().toUpperCase()

        firestoreDB!!.collection("notes").whereEqualTo("city",cityvarstart).orderBy("title")
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val notesList = mutableListOf<Note>()

                        for (doc in task.result!!) {
                            val note = doc.toObject<Note>(Note::class.java)
                            note.id = doc.id
                            notesList.add(note)
                        }
                        //progressBar.visibility = View.GONE
                        Log.d(TAG, "Progressbar GONE at line 126")

                        mAdapter = NoteRecyclerViewAdapter(notesList, applicationContext, firestoreDB!!)
                        val mLayoutManager = LinearLayoutManager(applicationContext)
                        rvNoteList.layoutManager = mLayoutManager
                        rvNoteList.itemAnimator = DefaultItemAnimator()
                        rvNoteList.adapter = mAdapter
                    } else {
                        //progressBar.visibility = View.GONE
                        Log.d(TAG, "Progressbar GONE at line 135")
                        Log.d(TAG, "Error getting documents: ", task.exception)
                        Toast.makeText(baseContext,"En feil oppstod, vennligst prøv på nytt", Toast.LENGTH_LONG).show()
                    }
                }
        notestrace2.stop()

    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            //Add note button
            if (item.itemId == R.id.addNote) {
                val intent = Intent(this, NoteActivityProfile::class.java)
                startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())}
            //MyprofileButton
            if (item.itemId == R.id.myProfile) {
                val intent = Intent(this, ProfileActivity::class.java)
                startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())}
        }
        return super.onOptionsItemSelected(item)
    }
}
