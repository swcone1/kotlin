package com.swc.stianstrange.kotlin

import android.app.Activity
import android.app.ActivityOptions
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.TextUtils
import android.util.Log
import android.view.Menu
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import com.google.firebase.FirebaseApp
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.inappmessaging.FirebaseInAppMessaging
import kotlinx.android.synthetic.main.logon.*

class MainActivity2 : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics
    private var FirebaseInAppMessaging: FirebaseInAppMessaging? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        FirebaseInAppMessaging?.setMessagesSuppressed(false)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.logon)
        buttonLogin.setOnClickListener(this)
        buttonForgot.setOnClickListener(this)
        buttonRegisterOnLogin.setOnClickListener(this)
        //Start firebase
        FirebaseApp.initializeApp(this)
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        }

        override fun onClick(v: View?) {
        val i = v?.id
        when (i) {
            R.id.buttonLogin -> signin(editTextEmail.text.toString(),editTextPassword.text.toString())
            R.id.buttonForgot -> forgot()
            R.id.buttonRegisterOnLogin -> register()
        }
    }

    private fun signin(email: String,password: String){
    Log.d(TAG, "signIn:$email")

        //Check form
        if (!validateForm()){
            return
        }
        loginProgress.visibility = View.VISIBLE
        buttonLogin.visibility = View.INVISIBLE
        //setloading()
        //keyboard hide

        val imm = getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
        //imm.toggleSoftInput(InputMethodManager.SHOW_IMPLICIT, 0)

        //Auth sign in
        auth.signInWithEmailAndPassword(email, password).addOnCompleteListener(this) {task ->
            if (task.isSuccessful){
                //LOG
                Log.d(TAG,"SIGN IN WITH EMAIL: SUCCESS")
                loginProgress.visibility = View.GONE
                buttonLogin.visibility = View.VISIBLE
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)

                //startActivity
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    val intent = Intent(this, MainActivity::class.java )
                   startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
                } else {
                    val intent = Intent(this, MainActivity::class.java )
                   startActivity(intent)
               }
                //user val
                val currentuser = auth.currentUser
                val currentuserUID = currentuser!!.uid
                //LOG
                Log.i(TAG,"FOLLOWING USER LOGGED IN")
                Log.i(TAG, currentuser.toString())
                Log.i(TAG, currentuserUID)
                Log.d(TAG,"NEW ACTIVITY STARTED")
                //buttonLogin.text = "Logg på"
            } else{
                Log.w(TAG, "SIGN IN WITH EMAIL: FAILURE",task.exception)
                Toast.makeText(baseContext,"Innlogging feilet.",Toast.LENGTH_SHORT).show()
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
                loginProgress.visibility = View.GONE
                buttonLogin.visibility = View.VISIBLE
            }
        }

    }

    private fun forgot(){

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val intent = Intent(this, Forgot::class.java )
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        } else {
            val intent = Intent(this, Forgot::class.java )
            startActivity(intent)
        }

    }

    private fun register(){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            val intent = Intent(this, Register::class.java )
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        } else {
            val intent = Intent(this, Register::class.java )
            startActivity(intent)
        }
    }

    fun validateForm(): Boolean {
        var valid = true
        val email = editTextEmail.text.toString()
        if (TextUtils.isEmpty(email)){
            editTextEmail.error = "Påkrevd"
            valid = false
        } else {
            editTextEmail.error = null
        }

        val password = editTextPassword.text.toString()
        if (TextUtils.isEmpty(password)) {
            editTextPassword.error = "Påkrevd"
            valid = false
        }
        else
        {
            editTextPassword.error = null
        }
        return valid
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)
        return true
    }

    companion object {
        private const val TAG = "FirebaseAuthLog"
    }
}


