package com.swc.stianstrange.kotlin

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.support.v4.content.ContextCompat
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.swc.stianstrange.kotlin.model.UserInfoPref
import com.google.android.gms.tasks.Continuation
import com.google.android.gms.tasks.Task
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageMetadata
import com.google.firebase.storage.UploadTask
import com.squareup.picasso.Picasso

import kotlinx.android.synthetic.main.activity_note_profile.*
import java.io.File

class NoteActivityProfile : AppCompatActivity() {

    val PERMISSION_REQUEST_CODE = 1001
    val PICK_IMAGE_REQUEST = 900
    lateinit var filePath : Uri
    private val TAG = "MyLog"
    private var firestoreDB: FirebaseFirestore? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics
    internal var id: String = ""
    internal var pictureuri = String()
    var mCurrentPhotoPath = String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_note_profile)

        //Firebase initialzation
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        firestoreDB = FirebaseFirestore.getInstance()

        var authcurrentuser = auth.currentUser.toString()

        //Logs
        Log.i(TAG,"FireStoreinstance: " + firestoreDB)
        Log.i(TAG,"FirebaseAuthInstance: " + auth)
        Log.i(TAG,"FirebaseAnalyticsInstance: " + analyt)
        Log.i(TAG, "Current logged in user : $authcurrentuser")

        val bundle = intent.extras
        if (bundle != null) {

            id = bundle.getString("UpdateNoteId")
            edtTitleProfile.setText(bundle.getString("UpdateNoteTitle"))
            edtContentProfile.setText(bundle.getString("UpdateNoteContent"))
            editCityProfile.setText(bundle.getString("UpdateNoteCity"))
            editPriceProfile.setText(bundle.getString("UpdateNotePrice"))
            if (bundle.getString("UpdateNotePicture").length < 4) {
                Log.w(TAG, "No picture path in bundle")
            } else {
                Picasso.with(this)
                    .load(bundle.getString("UpdateNotePicture"))
                    .resize(300, 300)
                    .centerCrop()
                    .rotate(90F)
                    .into(up)
                pictureuri=bundle.getString("UpdateNotePicture")
            }
        }

        val telephone = UserInfoPref.number
        val email = UserInfoPref.email

        btAddProfile.setOnClickListener {

            val title = edtTitleProfile.text.toString()
            val content = edtContentProfile.text.toString()
            val useruid = auth.currentUser?.uid.toString()
            val city = editCityProfile.text.toString()
            val price = editPriceProfile.text.toString()


            if (title.isNotEmpty()) {
                if (id.isNotEmpty()) {
                    updateNote(id, title, content,city, useruid, price,pictureuri,telephone,email)
                } else {
                    addNote(title, content, city, useruid, price,pictureuri,telephone,email)
                }
            }
            Log.d(TAG,"NoteActivityProfile finished")
            val intent = Intent(this, ProfileActivity::class.java)
            Log.d(TAG,"ProfileActivity created")
            startActivity(intent)
            finish()
        }

        button_choose_picture.setOnClickListener{
            when {
                (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) -> {
                    if (ContextCompat.checkSelfPermission(this@NoteActivityProfile, Manifest.permission.READ_EXTERNAL_STORAGE)
                        != PackageManager.PERMISSION_GRANTED) {
                        requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), PERMISSION_REQUEST_CODE)
                    }else{
                        chooseFile()
                    }
                }

                else -> chooseFile()
            }
        }

        button_take_picture.setOnClickListener{
            dispatchTakePictureIntent()

        }
    }

    val REQUEST_IMAGE_CAPTURE = 1

    private fun dispatchTakePictureIntent() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }
    }




    private fun chooseFile(){
        val intent = Intent().apply {
            type = "image/*"
            action = Intent.ACTION_GET_CONTENT
        }
        startActivityForResult(Intent.createChooser(intent, "Velg Bilde"), PICK_IMAGE_REQUEST)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode){
            PERMISSION_REQUEST_CODE -> {
                if(grantResults.isEmpty() || grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    //Toast.makeText(this@NoteActivityProfile, "Oops! Permission Denied!!",Toast.LENGTH_SHORT).show()
                else
                    chooseFile()
            }
        }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode != Activity.RESULT_OK) {
            return
        }
        when (requestCode){
            PICK_IMAGE_REQUEST -> {
                filePath = data!!.data
                uploadFile()

            }
        }
    }


    fun uploadFile() {
        val currentusr = auth.currentUser?.email
        val current = java.util.Calendar.getInstance()

        upprogress.visibility = View.VISIBLE
        upprogress.progress = 1
        val data = FirebaseStorage.getInstance()
        val ref = data.reference.child("pictures")
        // File or Blob
        var file = Uri.fromFile(File(filePath.path))

// Create the file metadata
        var metadata = StorageMetadata.Builder()
            .setContentType("image/jpeg")
            .build()

        var uploadTask = ref.child("$currentusr/").putFile(filePath)
        uploadTask.addOnProgressListener { taskSnapshot ->
            val progress = (100.0 * taskSnapshot.bytesTransferred) / taskSnapshot.totalByteCount
            upprogress.progress = progress.toInt()
        }
            .addOnPausedListener {
        }
            .addOnFailureListener {
            //Add crashlytics log here 
        }
            .addOnSuccessListener {
                upprogress.progress = 100
            val urlTask = uploadTask.continueWithTask(Continuation<UploadTask.TaskSnapshot, Task<Uri>> { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                }
                return@Continuation ref.child("$currentusr/$current/").downloadUrl
            })
                .addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    Toast.makeText(applicationContext, "Bilde er lastet opp", Toast.LENGTH_SHORT).show()
                    Log.d(TAG,"${downloadUri}")
                    button_choose_picture.isClickable = false
                    button_choose_picture.isEnabled = false
                    pictureuri = downloadUri.toString()
                    upprogress.visibility = View.GONE
                    Picasso.with(this)
                        .load(pictureuri)
                        .resize(300,300)
                        .centerCrop()
                        .rotate(90F)
                        .into(up)
                } else {
                    Toast.makeText(applicationContext, "En feil oppstod", Toast.LENGTH_SHORT).show()
                }
            }

        }

    }

    private fun updateNote(id: String, title: String, content: String, city: String, useruid: String, price: String, pictureuri: String, telephone: String, email: String) {
        val note = Note(id, title, content, city, useruid, price, pictureuri, telephone, email).toMap()
        val firestoredocumentid = firestoreDB!!.collection("notes").document(id)

        Log.d(TAG,"Update following document " + firestoredocumentid)
        firestoreDB!!.collection("notes")
                .document(id)
                .set(note)
                .addOnSuccessListener {
                    Log.e(TAG, "Note document update successful!")
                    Toast.makeText(applicationContext, "Produkt oppdatert!", Toast.LENGTH_SHORT).show()

                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "Error adding Note document", e)
                    Toast.makeText(applicationContext, "En feil oppstod, vennligst prøv igjen", Toast.LENGTH_SHORT).show()
                    //Add crashlytics log here 
                }
    }

    private fun addNote(title: String, content: String,city: String, useruid: String, price: String, pictureuri: String, telephone: String, email: String) {
        val note = Note(title, content,city, useruid, price, pictureuri, telephone, email).toMap()

        firestoreDB!!.collection("notes")
                .add(note)
                .addOnSuccessListener { documentReference ->
                    Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.id)
                    Toast.makeText(applicationContext, "Produktet er lagt ut til utlån", Toast.LENGTH_SHORT).show()

                }
                .addOnFailureListener { e ->
                    Log.e(TAG, "Error adding Note document", e)
                    //Add crashlytics log here 
                    Toast.makeText(applicationContext, "Prduktet kunne ikke legges til", Toast.LENGTH_SHORT).show()


                }
    }
}
