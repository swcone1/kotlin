package com.swc.stianstrange.kotlin

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.support.v7.widget.DefaultItemAnimator
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import com.chibatching.kotpref.Kotpref
import com.swc.stianstrange.kotlin.model.UserInfoPref
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.EventListener
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.ListenerRegistration
import kotlinx.android.synthetic.main.activity_profile.*

class ProfileActivity : AppCompatActivity(), View.OnClickListener {


    private val TAG = "MyLog"
    //RecyclerAdapter
    private var mAdapter: NoteRecyclerViewAdapterMyProfile? = null

    //FireStore
    private var firestoreDB: FirebaseFirestore? = null
    private var firestoreListener: ListenerRegistration? = null
    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics

    //Start logs

    public override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart() called")
    }

    public override fun onPause() {
        super.onPause()
        Log.d(TAG, "onPause() called")
    }


    public override fun onStop() {
        super.onStop()
        Log.d(TAG, "onStop() called")
        finish()
    }

    public override fun onDestroy() {
        super.onDestroy()
        Log.d(TAG, "onDestroy() called")
        firestoreListener!!.remove()
    }


    override fun onResume() {
        super.onResume()
        Log.d(TAG, "onResume() called")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)
        Log.d(TAG,"ActivityCreated")
        progressBar2.visibility = View.VISIBLE
        Kotpref.init(this)
        //Firebase initialzation
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        firestoreDB = FirebaseFirestore.getInstance()
        var authcurrentuser = auth.currentUser?.email.toString()
        Log.d(TAG, "Current user: $authcurrentuser")
        UserName.text = authcurrentuser
        updateUser.setOnClickListener(this)
        saveUser.setOnClickListener(this)
        loguserout.setOnClickListener(this)

        //Logs
        Log.i(TAG, "FireStoreinstance: $firestoreDB")
        Log.i(TAG, "FirebaseAuthInstance: $auth")
        Log.i(TAG, "FirebaseAnalyticsInstance: $analyt")
        Log.i(TAG, "Current logged in user : $authcurrentuser")

        //Get current list of products
        Log.d(TAG, "LoadNotesList() called")
        loadNotesList()
        Log.d(TAG, "firestorelistenerfuction() called")
        firestorelistenerfunction()
        UserInfoPref.email = auth.currentUser?.email!!
        name.text = UserInfoPref.userfirstname
        location.text = UserInfoPref.location
        number.text = UserInfoPref.number

    }

    override fun onClick(v: View?) {
        val i = v?.id
        when (i) {
            R.id.updateUser -> updateuser()
            R.id.saveUser -> saveuser()
            R.id.loguserout -> logout()
        }
    }

    fun updateuser(){
        name.visibility = View.GONE
        number.visibility = View.GONE
        location.visibility = View.GONE
        nameEdit.visibility = View.VISIBLE
        numberEdit.visibility = View.VISIBLE
        userLocationEdit.visibility = View.VISIBLE
        saveUser.isEnabled = true
    }

    fun logout(){
        auth.signOut()
        finishAffinity()

    }

    fun saveuser(){
        val nametext = nameEdit.text
        val numbertext = numberEdit.text
        val locationtext = userLocationEdit.text

        UserInfoPref.location = locationtext.toString()
        UserInfoPref.number = numbertext.toString()
        UserInfoPref.userfirstname = nametext.toString()
        Log.d(TAG, UserInfoPref.location)
        Log.d(TAG, UserInfoPref.number)
        Log.d(TAG, UserInfoPref.userfirstname)
        name.visibility = View.VISIBLE
        number.visibility = View.VISIBLE
        location.visibility = View.VISIBLE
        nameEdit.visibility = View.GONE
        numberEdit.visibility = View.GONE
        userLocationEdit.visibility = View.GONE
        saveUser.isEnabled = false
        name.text = UserInfoPref.userfirstname
        number.text = UserInfoPref.number
        location.text = UserInfoPref.location

    }


    fun firestorelistenerfunction(){
        val useruid = auth.currentUser?.uid.toString()
        firestoreListener = firestoreDB!!.collection("notes").whereEqualTo("useruid",useruid)
            .addSnapshotListener(EventListener { documentSnapshots, e ->
                if (e != null) {
                    Log.e(TAG, "Listen failed!", e)
                    return@EventListener
                }

                val notesList = mutableListOf<Note>()

                for (doc in documentSnapshots!!) {
                    val note = doc.toObject(Note::class.java)
                    note.id = doc.id
                    notesList.add(note)
                    Log.d(TAG, "message: $notesList")
                }

                mAdapter = NoteRecyclerViewAdapterMyProfile(notesList, applicationContext, firestoreDB!!)
                rvNoteListProfile.adapter = mAdapter
            })
    }

    private fun loadNotesList() {
        val useruid = auth.currentUser?.uid.toString()
        firestoreDB!!.collection("notes").whereEqualTo("useruid",useruid)
                .get()
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        val notesList = mutableListOf<Note>()
                        for (doc in task.result!!) {
                            val note = doc.toObject<Note>(Note::class.java)
                            note.id = doc.id
                            notesList.add(note)
                        }
                        progressBar2.visibility = View.GONE
                        mAdapter = NoteRecyclerViewAdapterMyProfile(notesList, applicationContext, firestoreDB!!)
                        val mLayoutManager = LinearLayoutManager(applicationContext)
                        rvNoteListProfile.layoutManager = mLayoutManager
                        rvNoteListProfile.itemAnimator = DefaultItemAnimator()
                        rvNoteListProfile.adapter = mAdapter
                        Log.d(TAG,"Notes Loaded and adapter is attached")
                        val itemcount = mAdapter?.itemCount
                        Log.d(TAG, "ItemCount on adapter: $itemcount")
                        if (itemcount != null) {
                            if (itemcount.equals(0)) {
                                textView4.visibility = View.VISIBLE
                            }
                        }
                    } else {
                        Log.d(TAG, "Error getting documents: ", task.exception)
                        Toast.makeText(baseContext,"En feil oppstod, vennligst prøv på nytt", Toast.LENGTH_LONG).show()
                    }
                }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_main, menu)

        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item != null) {
            //Add note button
            if (item.itemId == R.id.addNote) {
                val intent = Intent(this, NoteActivityProfile::class.java)
                Log.d(TAG,"NoteActivityProfile created")
                startActivity(intent) }
            //MyprofileButton
            if (item.itemId == R.id.myProfile) {
            }
        }
        return super.onOptionsItemSelected(item)
    }
}




