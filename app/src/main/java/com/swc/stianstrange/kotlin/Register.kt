package com.swc.stianstrange.kotlin

import android.app.ActivityOptions
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.widget.Toast
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.firestore.FirebaseFirestore
import kotlinx.android.synthetic.main.register.*

class Register : AppCompatActivity(), View.OnClickListener {

    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics
    private var firestoreDB: FirebaseFirestore? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.register)
        buttonRegister.setOnClickListener(this)
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        firestoreDB = FirebaseFirestore.getInstance()
    }

    override fun onClick (v: View?){
        val i=v?.id
        when (i){
            R.id.buttonRegister -> registeruser(editTextEmailRegister.text.toString(),editTextPasswordRegister.text.toString())

        }
    }
    private fun registeruser(email: String, password: String){
    Log.d(TAG,"CREATE ACCOUNT$email")
        if (!validateForm()){
            return
        }
        auth.createUserWithEmailAndPassword(email,password)
            .addOnCompleteListener(this){   task ->
                if (task.isSuccessful){
                    Log.d(TAG,"USER SUCCESSFULLY CREATED")
                    val user = auth.currentUser
                    user?.sendEmailVerification()
                    Log.d(TAG,"CURRENT USER")
                    Log.d(TAG,user.toString())
                    Toast.makeText(baseContext,"Brukeren er opprettet. Epost er sent til din bruker for godkjenning",Toast.LENGTH_LONG).show()

                    val userregistrated = auth.currentUser?.email.toString()
                    val useruid = auth.currentUser?.uid.toString()
                    val email = userregistrated
                    val age = 1
                    val firstname = "Default"
                    val lastname = "Default"
                    val city = "Default"
                    val phonenumber = 1
                    val profilepicture = "Default"

                    addusernote(
                        age,
                        email,
                        firstname,
                        lastname,
                        city,
                        phonenumber,
                        profilepicture,
                        useruid
                    )
                    finish()
                } else {
                    Log.w(TAG,"USETCREATION DID NOT COMPLEATE", task.exception)
                }
            }
    }

    private fun addusernote(age: Number, email: String, firstname: String, lastname: String, city: String, phonenumber: Number, profilepicture: String, useruid: String){
        val usernote = UserNote(age, email,firstname,lastname,city,phonenumber,profilepicture, useruid).toMap()
        firestoreDB!!.collection("Users")
            .add(usernote)
            .addOnSuccessListener { documentReference ->
                Log.e(TAG, "DocumentSnapshot written with ID: " + documentReference.id)
                Toast.makeText(applicationContext, "Din bruker er opprettet", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener { e ->
                Log.e(TAG, "Error adding Note document", e)
                Toast.makeText(applicationContext, "Feil ved oppretting av din bruker", Toast.LENGTH_SHORT).show()
            }
            .addOnFailureListener{ w ->
                Toast.makeText(applicationContext, w.message, Toast.LENGTH_LONG).show()
            }
    }

    fun start () {
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())
        }


    private fun validateForm(): Boolean {
        var valid = true
        val email = editTextEmailRegister.text.toString()
        if (TextUtils.isEmpty(email)){
            editTextEmailRegister.error = "Påkrevd"
            valid = false
        } else {
            editTextEmailRegister.error = null
        }

        val password = editTextPasswordRegister.text.toString()
        if (TextUtils.isEmpty(password)) {
            editTextPasswordRegister.error = "Påkrevd"
            valid = false
        }
        else
        {
            editTextPasswordRegister.error = null
        }
        if (password.length >= 8){
            editTextPasswordRegister.error = null
            valid = true
        } else {
            editTextPasswordRegister.error = "Passord må inneholde minst 8 tegn"
            Toast.makeText(baseContext,"Passord må inneholde minst 8 tegn", Toast.LENGTH_LONG).show()
        }

        return valid
    }

    companion object {
        private const val TAG = "FirebaseAuthLog"
    }
}
