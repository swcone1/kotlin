package com.swc.stianstrange.kotlin

import android.app.ActivityOptions
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.inappmessaging.FirebaseInAppMessaging
import kotlinx.android.synthetic.main.splash.*
import java.util.*

class SplashActivity : AppCompatActivity() {

    private lateinit var auth: FirebaseAuth
    private lateinit var analyt: FirebaseAnalytics
    private var FirebaseInAppMessaging: FirebaseInAppMessaging? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        FirebaseInAppMessaging?.setMessagesSuppressed(true)

        setContentView(R.layout.splash)
        super.onCreate(savedInstanceState)
        progressBar3.visibility = View.VISIBLE
        auth = FirebaseAuth.getInstance()
        analyt = FirebaseAnalytics.getInstance(this)
        val user = auth.currentUser
        Handler().postDelayed({navigate(user = user)},100)
        Log.d(TAG, "Current user logged in is $user")


    }


    private fun navigate (user: FirebaseUser?){
        if (user == null) {
            Log.d(TAG,"Started main activity")
            val intent = Intent(this, MainActivity2::class.java)
            startActivity(intent, ActivityOptions.makeSceneTransitionAnimation(this).toBundle())

        } else {
            Log.d(TAG, "Started NavActivity")
            val intent = Intent(this, MainActivity::class.java )
            startActivity(intent,ActivityOptions.makeSceneTransitionAnimation(this).toBundle())

        }
        progressBar3.visibility = View.GONE
        finish()
    }
    companion object {
        private const val TAG = "FirebaseAuthLog"
    }
}
