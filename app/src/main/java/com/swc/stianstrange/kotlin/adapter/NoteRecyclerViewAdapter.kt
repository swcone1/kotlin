package com.swc.stianstrange.kotlin

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.google.firebase.firestore.FirebaseFirestore

import android.content.Intent
import android.support.v7.widget.CardView
import android.util.Log
import com.squareup.picasso.Picasso

class NoteRecyclerViewAdapter(
        private val notesList: MutableList<Note>,
        private val context: Context,
        private val firestoreDB: FirebaseFirestore,
        private val TAG: String = "MyLog")
    : RecyclerView.Adapter<NoteRecyclerViewAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = notesList[position]
        holder.title.text = note.title
        holder.content.text = note.content
        holder.city.text = note.city
        holder.price.text = note.price
        if (note.pictureuri.equals("")) {
        }else
            Picasso.with(context)
                .load(note.pictureuri)
                .resize(300,300)
                .centerCrop()
                .rotate(90F)
                .into(holder.picture)
        holder.title.setOnClickListener{showNote(note)}
        holder.content.setOnClickListener{showNote(note)}
        holder.city.setOnClickListener{showNote(note)}
        holder.price.setOnClickListener{showNote(note)}
        holder.picture.setOnClickListener{showNote(note)}
        holder.cardview.setOnClickListener{showNote(note)}
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var title: TextView = view.findViewById(R.id.tvTitle)
        internal var content: TextView = view.findViewById(R.id.tvContent)
        internal var city: TextView = view.findViewById(R.id.tvCity)
        internal var price: TextView = view.findViewById(R.id.tvPrice)
        internal var picture: ImageView = view.findViewById(R.id.tvPicture)
        internal var cardview: CardView = view.findViewById(R.id.cardView)
    }

    private fun showNote(note: Note){
        Log.d(TAG, "Note" + note.id + " clicked")
        val intent = Intent(context, BigProductActivity::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            putExtra("ShowNoteId",note.id)
            putExtra("ShowNoteTitle",note.title)
            putExtra("ShowNoteContent",note.content)
            putExtra("ShowNoteCity",note.city)
            putExtra("ShowNotePrice",note.price)
            putExtra("showNotePicture",note.pictureuri)
            putExtra("ShowNoteEmail",note.email)
            putExtra("ShowNoteTelephone",note.telephone)
        }
        context.startActivity(intent)
    }
}

