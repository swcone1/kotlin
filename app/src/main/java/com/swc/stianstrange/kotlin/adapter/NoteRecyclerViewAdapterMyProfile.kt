package com.swc.stianstrange.kotlin

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView

import com.google.firebase.firestore.FirebaseFirestore

import android.content.Intent
import android.util.Log
import android.widget.Toast
import com.squareup.picasso.Picasso

class NoteRecyclerViewAdapterMyProfile(
        private val notesList: MutableList<Note>,
        private val context: Context,
        private val firestoreDB: FirebaseFirestore,
        private val TAG: String = "MyLog")

    : RecyclerView.Adapter<NoteRecyclerViewAdapterMyProfile.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_note_profile, parent, false)
        Log.d(TAG,"ViewHolder Created")
        return ViewHolder(view)
    }



    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val note = notesList[position]

        holder.title.text = note.title
        holder.content.text = note.content
        holder.city.text = note.city
        holder.price.text = note.price
        if (note.pictureuri.equals("")) {
        }else
            Picasso.with(context)
                .load(note.pictureuri)
                .resize(300,300)
                .centerCrop()
                .rotate(90F)
                .into(holder.picture)
        holder.edit.setOnClickListener { updateNotev2(note) }
        holder.delete.setOnClickListener { deleteNote(note.id!!, position) }
        Log.d(TAG,"Viewholder Binded")
    }

    override fun getItemCount(): Int {
        return notesList.size
    }

    inner class ViewHolder internal constructor(view: View) : RecyclerView.ViewHolder(view) {
        internal var title: TextView
        internal var content: TextView
        internal var city: TextView
        internal var price: TextView
        internal var edit: ImageView
        internal var delete: ImageView
        internal var picture: ImageView
        init {
            title = view.findViewById(R.id.tvTitleProfile)
            content = view.findViewById(R.id.tvContentProfile)
            price = view.findViewById(R.id.tvPriceProfile)
            city = view.findViewById(R.id.tvCityProfile)
            picture = view.findViewById(R.id.tvPictureProfile)
            edit = view.findViewById(R.id.ivEditProfile)
            delete = view.findViewById(R.id.ivDelete)
            Log.d(TAG,"ViewHolder check")
        }
    }

    private fun updateNotev2(note: Note){
        Log.d(TAG,"Edit button clicked")
        val intent = Intent(context, NoteActivityProfile::class.java).apply {
            addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            putExtra("UpdateNoteId",note.id)
            putExtra("UpdateNoteTitle",note.title)
            putExtra("UpdateNoteContent",note.content)
            putExtra("UpdateNoteCity",note.city)
            putExtra("UpdateNotePrice",note.price)
            putExtra("UpdateNotePicture",note.pictureuri)
        }
        Log.d(TAG,"Starting NoteActivityProfile with following extras: "+ intent.extras + intent.flags)
        context.startActivity(intent)
    }


    private fun deleteNote(id: String, position: Int) {
        firestoreDB.collection("notes")
                .document(id)
                .delete()
                .addOnCompleteListener {
                    notesList.removeAt(position)
                    notifyItemRemoved(position)
                    notifyItemRangeChanged(position, notesList.size)
                    Toast.makeText(context, "Produktannonse er fjernet", Toast.LENGTH_SHORT).show()
                }
    }
}
