package com.swc.stianstrange.kotlin

import java.util.HashMap

class Note {

    var id: String? = null
    var title: String? = null
    var content: String? = null
    var city: String? = null
    var useruid: String? = null
    var price: String? = null
    var pictureuri: String? = null
    var telephone: String? = null
    var email: String? = null

    constructor()

    constructor(id: String, title: String, content: String, city: String, useruid: String, price: String, pictureuri: String, telephone: String, email: String) {
        this.id = id
        this.title = title
        this.content = content
        this.city = city
        this.useruid = useruid
        this.price = price
        this.pictureuri = pictureuri
        this.telephone = telephone
        this.email = email

    }

    constructor(title: String, content: String,city: String, useruid: String, price: String, pictureuri: String, telephone: String, email: String)
    {
        this.title = title
        this.content = content
        this.city = city
        this.useruid = useruid
        this.price = price
        this.pictureuri = pictureuri
        this.telephone = telephone
        this.email = email
    }

    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result.put("title", title!!)
        result.put("content", content!!)
        result.put("city", city!!.toUpperCase())
        result.put("useruid", useruid!!)
        result.put("price", price!!)
        result.put("pictureuri", pictureuri!!)
        result.put("telephone", telephone!!)
        result.put("email", email!!)

        return result
    }
}