package com.swc.stianstrange.kotlin.model

import com.chibatching.kotpref.KotprefModel

object UserInfoPref : KotprefModel(){
    var userfirstname by stringPref()
    var age by intPref()
    var location by stringPref()
    var number by stringPref()
    var email by stringPref()

}