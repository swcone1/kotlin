package com.swc.stianstrange.kotlin

import java.util.HashMap

class UserNote {
    var id: String? = null
    var age: Number? = null
    var email: String? = null
    var firstname: String? = null
    var lastname: String? = null
    var city: String? = null
    var phonenumber: Number? = null
    var profilepicture: String? = null
    var useruid: String? = null

    constructor()

    constructor(id: String, age: Number, email: String, firstname: String, lastname: String, city: String, phonenumber: Number, profilepicture: String, useruid: String)
    {
        this.id = id
        this.age = age
        this.email = email
        this.firstname = firstname
        this.lastname = lastname
        this.city = city
        this.phonenumber = phonenumber
        this.profilepicture = profilepicture
        this.useruid = useruid

    }

    constructor(age: Number, email: String, firstname: String, lastname: String, city: String, phonenumber: Number, profilepicture: String, useruid: String)
    {
        this.age = age
        this.email = email
        this.firstname = firstname
        this.lastname = lastname
        this.city = city
        this.phonenumber = phonenumber
        this.profilepicture = profilepicture
        this.useruid = useruid
    }


    fun toMap(): Map<String, Any> {

        val result = HashMap<String, Any>()
        result.put("age", age!!)
        result.put("email", email!!)
        result.put("firstname", firstname!!)
        result.put("lastname", lastname!!)
        result.put("city", city!!)
        result.put("phonenumber", phonenumber!!)
        result.put("profilepicture", profilepicture!!)
        result.put("useruid",useruid!!)



        return result
    }
}